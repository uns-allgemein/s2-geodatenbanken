import json
import psycopg2

# connect to DB
con = psycopg2.connect(database="geodb", user="mike", password="geodb", host="195.128.101.62", port="45432")
cur = con.cursor()

# function to avoid KeyErrors
def getValue(dict, key):
    try:
        return dict[key]
    except:
        return ''

def composeValues(feature, array):
    outValues = ""
    for item in array:
        outValues = outValues + "'"+getValue(feature['properties'],item)+"',"
    return(outValues)

def unionTables(cur, array):
    for item in array:
        cur.execute("INSERT INTO gebieteunion"
            "(tablename, geombuffer)"
            "SELECT '"+item+"', ST_Multi(ST_Union(ST_Buffer(geom::geography, 100)::geometry)) FROM "+item)

# Load amenity
geoJSON = "./geojson/amenity.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['name', 'phone'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO amenity "
    "(name, phone, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load highway
geoJSON = "./geojson/highway.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['highway', 'maxspeed', 'name', 'ref', 'surface'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO highway "
    "(highway, maxspeed, name, ref, surface, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load landuse
geoJSON = "./geojson/landuse.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['name', 'landuse'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO landuse "
    "(name, landuse, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load leisure
geoJSON = "./geojson/leisure.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['name', 'url'])
        values = values + "ST_Multi(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')),"
        values = values + "ST_Multi(ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry)"
        values = values + "),"
cur.execute("INSERT INTO leisure "
    "(name, url, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load military
geoJSON = "./geojson/military.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['name', 'military'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO military "
    "(name, military, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load power
geoJSON = "./geojson/power.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['voltage', 'cables', 'operator'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO power "
    "(voltage, cables, operator, geom, geombuffer) "
    "VALUES"+values[:-1])

# Load railway
geoJSON = "./geojson/railway.geojson"
inJSON = json.loads(open(geoJSON, "r").read())
values = ""
for feature in inJSON['features']:
    if 'geometry' in feature:
        values = values + "("
        values = values + composeValues(feature, ['name', 'operator', 'owner', 'tracks', 'electrified'])
        values = values + "ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"'),"
        values = values + "ST_Buffer(ST_GeomFromGeoJSON('"+str(feature['geometry']).replace("'",'"')+"')::geography, 100)::geometry"
        values = values + "),"
cur.execute("INSERT INTO railway "
    "(name, operator, owner, tracks,electrified, geom, geombuffer) "
    "VALUES"+values[:-1])

unionTables(cur, ['amenity', 'highway', 'landuse', 'leisure', 'military', 'power', 'railway'])


con.commit()
con.close()