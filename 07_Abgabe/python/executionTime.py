import psycopg2

# connect to DB
con = psycopg2.connect(database="geodb", user="mike", password="geodb", host="195.128.101.62", port="45432")
cur = con.cursor()
executionTime = []
for i in range(100):
    cur.execute("EXPLAIN ANALYZE "
        #"SELECT json_build_object('type', 'Feature','id', id,'geometry', ST_AsGeoJSON(ST_Buffer(geom::geography, 100)::geometry)::json,'properties', json_build_object('highway', highway,'maxspeed', maxspeed,'name', name,'ref',ref,'surface',surface)) FROM highway WHERE ST_Contains(ST_Buffer(geom::geography, 100)::geometry,ST_GeomFromText('POINT(8.745830 48.714406)',4326))"#100 - 166.356ms
        #"SELECT json_build_object('type', 'Feature','id', id,'geometry', ST_AsGeoJSON(geombuffer)::json,'properties', json_build_object('highway', highway,'maxspeed', maxspeed,'name', name,'ref',ref,'surface',surface)) FROM highway WHERE ST_Contains(geombuffer,ST_GeomFromText('POINT(8.745830 48.714406)',4326))"#100 - 0.629
        "SELECT tablename, ST_Contains(geombuffer,ST_GeomFromText('POINT(8.745830 48.714406)',4326)) FROM gebieteunion" #100 2.3367
        )
    executionTime.append(float(str(cur.fetchall()[-1]).replace("('Execution Time: ","").replace(" ms',)","")))
print(sum(executionTime)/len(executionTime))
con.close()