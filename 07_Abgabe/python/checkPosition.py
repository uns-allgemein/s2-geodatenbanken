import psycopg2

con = psycopg2.connect(database="geodb", user="mike", password="geodb", host="195.128.101.62", port="45432")
cur = con.cursor()

def getGeoJSON(cur, fields, table, position):
    cur.execute("""SELECT json_build_object(
        'type', 'FeatureCollection',
        'features', json_agg(
            json_build_object(
                'type', 'Feature',
                'id', id,
                'geometry', ST_AsGeoJSON(geombuffer)::json,
                'properties', json_build_object(
                    """+fields+"""
                )
            )
        )
    ) FROM """+table+"""
    WHERE ST_Contains(ST_Buffer(geom::geography, 100)::geometry,ST_GeomFromText('POINT("""+position+""")',4326))""")
    results = cur.fetchall() 
    # check if the result is a list - true create a geojson - false do nothing
    if type(results[0][0]['features']) == list:
        print('Attention:',table)
        f = open("outGeoJSON/"+table+".geojson", "w")
        f.write(str(results[0][0]).replace("'",'"'))
        f.close()

def checkPosition(position):
    #list of tables with values to create SQL-SELECT
    getGeoJSON(cur, "'name',name,'phone',phone","amenity",position)
    getGeoJSON(cur, "'highway',highway,'maxspeed',maxspeed,'name',name,'ref',ref,'surface',surface","highway",position)
    getGeoJSON(cur, "'name',name,'landuse',landuse","landuse",position)
    getGeoJSON(cur, "'name',name,'url',url","leisure",position)
    getGeoJSON(cur, "'name',name,'military',military","military",position)
    getGeoJSON(cur, "'voltage',voltage,'cables',cables,'operator',operator","power",position)
    getGeoJSON(cur, "'name',name,'operator',operator,'owner',owner,'tracks',tracks,'electrified',electrified","railway",position)

checkPosition("8.746749 48.714454")

con.close()