import psycopg2

con = psycopg2.connect(database="geodb", user="mike", password="geodb", host="195.128.101.62", port="45432")

cur = con.cursor()
# Delete existing tables
cur.execute('''DROP TABLE IF EXISTS amenity, gebieteunion, highway, landuse, leisure, military, power, railway''')
con.commit()

#Create Tables
cur.execute('''CREATE TABLE amenity (
        id SERIAL PRIMARY KEY,
        name VARCHAR(80),
        phone VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','amenity','geom',4326,'POLYGON',2);
    SELECT AddGeometryColumn('public','amenity','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_amenity_geom ON amenity USING GIST (geom);
    CREATE INDEX idx_amenity_geombuffer ON amenity USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE highway (
        id SERIAL PRIMARY KEY,
        highway VARCHAR(80),
        maxspeed VARCHAR(80),
        name VARCHAR(80),
        ref VARCHAR(80),
        surface VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','highway','geom',4326,'LINESTRING',2);
    SELECT AddGeometryColumn('public','highway','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_highway_geom ON highway USING GIST (geom);
    CREATE INDEX idx_highway_geombuffer ON highway USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE landuse (
        id SERIAL PRIMARY KEY,
        name VARCHAR(80),
        landuse VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','landuse','geom',4326,'POLYGON',2);
    SELECT AddGeometryColumn('public','landuse','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_landuse_geom ON landuse USING GIST (geom);
    CREATE INDEX idx_landuse_geombuffer ON landuse USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE leisure (
        id SERIAL PRIMARY KEY,
        name VARCHAR(80),
        url VARCHAR(255)
    );
    SELECT AddGeometryColumn('public','leisure','geom',4326,'MULTIPOLYGON',2);
    SELECT AddGeometryColumn('public','leisure','geombuffer',4326,'MULTIPOLYGON',2);
    CREATE INDEX idx_leisure_geom ON leisure USING GIST (geom);
    CREATE INDEX idx_leisure_geombuffer ON leisure USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE military (
        id SERIAL PRIMARY KEY,
        name VARCHAR(80),
        military VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','military','geom',4326,'POLYGON',2);
    SELECT AddGeometryColumn('public','military','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_military_geom ON military USING GIST (geom);
    CREATE INDEX idx_military_geombuffer ON military USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE power (
        id SERIAL PRIMARY KEY,
        voltage VARCHAR(80),
        cables VARCHAR(80),
        operator VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','power','geom',4326,'LINESTRING',2);
    SELECT AddGeometryColumn('public','power','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_power_geom ON power USING GIST (geom);
    CREATE INDEX idx_power_geombuffer ON power USING GIST (geombuffer);''')

cur.execute('''CREATE TABLE railway
 (
        id SERIAL PRIMARY KEY,
        name VARCHAR(80),
        operator VARCHAR(80),
        owner VARCHAR(80),
        tracks VARCHAR(80),
        electrified VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','railway','geom',4326,'LINESTRING',2);
    SELECT AddGeometryColumn('public','railway','geombuffer',4326,'POLYGON',2);
    CREATE INDEX idx_railway_geom ON railway USING GIST (geom);
    CREATE INDEX idx_railway_geombuffer ON railway USING GIST (geombuffer);''')



cur.execute('''CREATE TABLE gebieteunion (
        id SERIAL PRIMARY KEY,
        tablename VARCHAR(80)
    );
    SELECT AddGeometryColumn('public','gebieteunion','geombuffer',4326,'MULTIPOLYGON',2);
    CREATE INDEX idx_gebieteunion_geom ON gebieteunion USING GIST (geombuffer);''')

con.commit()
con.close()