\section{Anfragebearbeitung}
\subsection{Anforderungen}

Geodatenbanken müssen folgende Charakteristika berücksichtigen:
\begin{itemize}
    \item Die Anzahl der Objekte in einer Geodatenbank kann sehr groß werden.
    \item Die Verarbeitung von geometrischen Basisdaten wie Streckenzüge und Polygone sind komplex.
    \item Veränderungen durch nachträgliche Korrekturen, sowie Änderungen in der realen Welt oder das Hinzufügen von abgeleiteten Daten müssen berücksichtigt werden.
    \item Geoobjekte besitzen eine große Variabilität (eine sehr hohe sowie zeitgleich an anderen Positionen sehr geringe Menge).
\end{itemize}
Moderne Geodatenbanken müssen die folgenden Kriterien berücksichtigen:
\begin{itemize}
    \item Effektivität: Verwaltung von komplexen strukturierten geometrischen Objekten
    \item Effizienz: kurze Antwortzeiten für Anfragen, schnelles Einfügen, Ändern oder Entfernen
    \item Skalierbarkeit: Verwaltung sehr großer Datenmengen, die möglicherweise im betrieb der Datenbank noch wachsen; Anbindung vieler Benutzer
\end{itemize}

Um diesen Anforderungen gerecht zu werden bedient man sich folgenden Mitteln:
\begin{itemize}
    \item mehrstufige Anfragebearbeitung
    \item Reduzierung der Beschreibungskomplexität durch Approximation (einfachere Darstellung der Objekte).
    \item Verwendung geeigneter Zugriffsstrukturen (Indizes)
\end{itemize}

\subsection{Mehrstufige Anfragebearbeitung}
Ziel ist es möglichst viele Fehltreffer frühzeitig auszuscheiden, dies erreicht man durch eine schrittweise reduzierung der Treffermenge.
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-2.png}
        \caption{Prinzip der mehrstufigen Anfragebearbeitung mit n Filterschritten}
    \end{minipage}
\end{figure}

\subsection{Filterung über Approximationen}
Die Schnittbestimmung zwischen zwei polygonen kann sehr aufwenig sein, die exakte Geometrie wird mit Hilfe einfacher Geometrien und in den ersten Filterschritten mit dieser Geometrie gearbeitet.

\begin{itemize}
    \item Je komplexer die Approximation, desto besser ist die Approximationsgüte und desto wirksamer ist der entsprechende Filterschritt.
    \item Je komplexer die Approximation, desto aufwendiger ist die Berechnung und der Verarbeitungsaufwand und desto mehr Speicherplatz benötigt sie.
\end{itemize}

In der Praxis hat sich das minimal umgebende Rechteck (MUR, engl. Minimal Bounding Rectangle(MBR)) durchgesetzt, dessen parallel zu den Achsel des Koordinatensystems können leicht durch Minimum- und Maximum-Berechnungen ermittelt werden.\newline
Es wird auch zwischen konservativer und progressiver Approximation unterschieden.
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-5.png}
        \caption{Konservative und progressive Approximationen}
    \end{minipage}
\end{figure}
Eine mehrelementige Approximation wird meist für Rasterdaten oder MuliPolygone verwendet, es gilt:
\begin{itemize}
    \item Je mehr Elemente verwendet werden, desto besser ist die Approximationsgüte und desto wirksamer ist der entsprechende Filterschritt.
    \item Je mehr Elemente verwendet werden, desto höher ist der Verarbeitungsaufwand und desto mehr Speicherplatz benötigt die Approximation.
\end{itemize}
Es empfielt sich die Approximation der Geometrie durch achsenparallele minimal umgebende Rechtecke. Für diese Rechteckt wird dann ein Index berechnet. Über den Index wird in der ersten Filterstufe die Menge der möglichen Ergebniskandidaten einer Anfrage ermittelt. In der zweiten Filterstufe wird das exakte Ergebnis über geometrische Verfahren berechnet.

\subsection{Indexierung in Datenbanken / B-Bäume}
Generell ist ein Index ein dynamisches Inhaltsverzeichnis, das die schnelle Suche nach Datenblöcken mit Datensätzen unterstützt, die die Anfragebedingungen erfüllen.\newline
\newline
Es gilt:
\begin{itemize}
    \item für das indexierte Attribut bzw. für die indexierte Attributkombination beschleunigt der Index die Bearbeitung von Anfragen,
    \item Eingefüge- und andere Änderungsoperation werden hingegen langsamer,
    \item es erhöht sich der Speicherplatzbedarf.
\end{itemize}
Indexstrukturen werden oft über Suchbäume erzeugt. Durch diese können schnell große Datenmengen durchsucht werden, wobei bei Manchen Implementierungen auch einem Knoten mehrere Datensätze zugeordnet werden können.\newline
\newline
Typischerweise werden B-Bäume verwendet, diese sind balanciert, d.h. die Höhe des Baumes ist für Endknoten (Blätter) gleich.\newline
\newline
Ein B-Baum der Ordnung m ist ein Suchbaum mit folgenden Eigenschaften:
\begin{enumerate}
    \item Jeder Knoten enthält höchstens 2m Einträge.
    \item Jeder Knoten außer der Wurzel enthält mindestens m Einträge.
    \item Ein Knoten mit x Einträgen hat als innerer Knoten genau x+1 direkte Nachfahren (Sohnknoten) oder als Blattknoten keine Nachfahren.\newline
    Die Anzahl der direkten Nachkommen wird Verzweigungsgrad des Knotens genannt. Der maximale Verzweigungsgrad beträgt damit 2m+1.
    \item Alle Blätter befinden sich auf dem gleichen Niveau, d.h. die Weglänge von den Wurzel zu allen Blättern ist gleich.
\end{enumerate}
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-9.png}
        \caption{B-Baum der Ordnung 2 (m=2)}
    \end{minipage}
\end{figure}
Vom B-Baum gibt es Varianten wie den B+-Baum, hier werden die Daten nur in den Blättern gespeichert.\newline
\newline
Alle Suchbäume haben eine lineare Ordnung, diese ist für Zahlen oder Zeichenketten offenkundig gegeben, für geometrische Datentypen hingegen nicht. Daher können herkömmliche Indexstrukturen nicht (ohne Weiteres) zur Indexierung von Geodaten eingesetzt werden.

\subsection{Räumliche Indexstrukturen}
Räumliche Indexe (engl. Spatial Access Methods(SAM)) dienen dazu, die Kandidaten zu bestimmen, die potenziell eine räumliche Anfragebedingung erfüllen.\newline
\newline
In der Literatur wird zwischen ``Space-Driven SAM'' und ``Data-Driven SAM'' unterschieden. Diese sind nicht nur für Geodatenbanken interessant, sondern auch für Software, die eine hohe Anzahl an grafischen Elementen verwalten, wie CAD-Software oder auch Computerspiele.\newline
\newline
Beim ``Space-Driven SAM'' wird der Datenraum unabhängig von den Daten zerlegt und indiziert.\newline
Wobei beim ``Data-Driven SAM'' der Index an Hand der Daten aufgebaut wird, welches effektiver ist.

\subsubsection{Regelmäßige Gitter (Fixed Grid)}
Die einfachste Idee ist die Zerlegung mit einem regelmäßigen Gitter. Die Gitterrechtzellen erhalten Adressen, die sich leicht aus Koordinaten berechnen lassen. Jedes geometrische Element erhält als Indexeintrag die Nummer des/der von ihm belegten Gitterzellen, bzw. verweist auf einen zugehörigen Speicher.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-10.png}
        \caption{Fixed Grid}
    \end{minipage}
\end{figure}
Ein Problem ist, dass die Anzahl der speicherbaren Einträge überlaufen kann. Dies lässt sich durch Überlaufbereiche beheben, die dann bei Bedarf angehängt werden.\newline
\newline
Regelmäßige Gitter sind als Datenindex prinzipiell geeignet, aber sie sind kein guter Index. Zum einen fällt es schwer, eine sinnvolle Größe der Gitterzellen zu berechnen, zum anderen ist die Dauer des Suchvorgangs abhängig von der Dichte der geometrischen Objekte. ISt bei der Datenbankanfrage die Gitterzelle berechnet, müssen im zweiten Schritt alle Objekte der Zelle durchsucht werden.

\subsubsection{Baumstrukturen}
Mit KD-Bäumen wird vorgeschlagen, die Datenebene im Wechsel horizontal und vertikal zu teilen, wenn eine bestimmte Anzahl von geometrischen Objekten enthalten ist. Dadurch ist gewährleistet, dass nach der Bestimmung der Zelle, die weitere Suche in einer erwarteten maximalen Zeit abgeschlossen ist.\newline
\newline
Bei einer großen Datendichte wird der Baum sehr hoch und die Suche im baum (erste Filterstufe) dauert sehr lange.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-13.png}
        \caption{Aufteilung von Punktobjekten nach den Regeln eines KD- Baums}
    \end{minipage}
\end{figure}

\subsubsection{PR-Quadtree}
PR-Quadtrees teilen die Ebene in Quadraten auf, sodass jedes Objekte durch genau ein Quadrat beschrieben ist (oder in Abbildung XX durch 7).\newline
\newline
Die Adresse des Objektes lässt sich binär kodieren, in dem man den Richtungen einen Binärcode gibt. Wird der Baum tiefer, dann verlängert man die Adresse.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-15.png}
        \caption{Bucket PR-Quadtrees mit maximal 7 Punkten pro Blattzelle}
    \end{minipage}
\end{figure}

\subsubsection{Adressfelder}
Adressfelder ordnen dem Datenraum ein Adressraum in Form einer Matrix zu. Über Errechnung der Position im Adressraum lässt sich der Index für die Objekte finden. Der Index selbst ist ein ``normaler'' Datenbankindex. Der große Speicherplatzbedarf sowie die Speicherung der Matrixpositionen welche selbst wenn wenige Elemente vorhanden sind müssen belegt werden sind die Nachteile der Adressfelder.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-16.png}
        \caption{Methode der Adressfelder: Datenraum (oben) und Adressraum (unten)}
    \end{minipage}
\end{figure}

\subsection{R-Bäume}
Dier R-Baum (engl. R-Tree) ist eine räumliche Indexstruktur, die zwei- oder mehrdimensionale Rechtecke mit Hilfe überlappender Blockregionen auf dem Hintergrundspeicher organisiert.\newline
\newline
Moderne Geodatenbanken verwenden R-Bäume oder Varianten als Basis für den räumlichen Index.\newline
\newline
Die Geometrien werden durch MUR genährt. Die MUR werden durch übergeordnete MUR so zusammengefasst, dass möglichst kleine übergeordnete MUR entstehen. der R-Baum ist ein balancierter Baum, alle Datenknoten weisen den gleichen Abstand zur Wurzel auf.\newline
\newline
Für die erste Filterstufe einer Datenbankanfrage wird der Baum von der Wurzel beginnend durchlaufen, bis die betreffende MUR gefunden wurde. Dann erfolgt wie bei den anderen Verfahren die geometrische Feinprüfung.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-20.png}
        \caption{Prinzip des R-Baums}
    \end{minipage}
\end{figure}
Für R-Bäume gelten folgende Regeln:
\begin{enumerate}
    \item Alle Blätter haben zwischen m (min. Kapazität) und M (max. Kapazität) Indexeinträge (m <= M/2)
    \item Für jeden Index- Eintrag (I, id) in einem Blatt ist I das kleinste umgebende Rechteck, das das Datenobjekt beinhaltet. id verweist auf den Datensatz.
    \item Jeder Knoten, der kein Blattknoten ist, hat zwischen m und M Söhne.
    \item Für jeden Eintrag in einem Knoten, der kein Blattknoten ist, ist das kleinste Rechteck das, das alle Rechtecke der Kindknoten beinhaltet.
    \item Die Wurzel hat mindestens zwei Söhne.
    \item Alle Blätter erscheinen auf derselben Höhe.
\end{enumerate}
Ist die maximale Knotenkapazität erreicht, müssen die Knoten geteilt werden. Die sogenannte Splitverfahren zielen darauf ab möglichst kleine Rechtecke mit wenig Überlappung zu erhalten.\newline
\newline
Der R+-Baum hat die gleiche Struktur wie der R-Baum, lässt jedoch keine Überlappungen zu. Um diese zu vermeiden werden überlappende Blattknoten mehrfach eingetragen.\newline
\begin{figure}[H]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=\linewidth]{../04_Anfragebearbeitung/inc/abb-25.png}
        \caption{Suchen im R+-Baum}
    \end{minipage}
\end{figure}

\subsection{Zusammenfassung}
Anfragebearbeitung bei Datenbanken und speziell bei Geodatenbanken erfordert eine sehr gute Indexierung, damit aus großen Datenmengen schnell Ergebnisse gefunden werden. Während normale Datenbanken Indizes verwenden, die auf Sortierung in linearer Reihenfolge basieren, benötigen Geodatenbanken spezielle mehrdimensionale Indizes.\newline
\newline
Als Ergebnis der Forschung der letzten Jahrzehnte haben sich mehrstufige (meist zweistufige) Filterverfahren als ideal bewiesen, die in de ersten Filterstufe die als minimal umschließendes achsenparalleles Rechteck genährten Geometriedaten in einem auf R-Bäume basiertes Verfahren verwalten. In der zweiten Filterstufe sind geometrische Feinprüfungen durchzuführen, um das exakte Ergebnis zu bestimmen.







\subsection{Fragen}
\subsubsection{Frage 1}
Was versteht man unter Skalierbarkeit einer Datenbank?\newline
\newline
Unter Skalierbarkeit versteht man die Möglichkeit die Geodatenbank an den Bedarf anzupassen. Verwendung stärkerer Server, mehrere Server oder anders weitige verbesserungen der Zugriffsgeschwindigkeiten.

\subsubsection{Frage 2}
Worin bestehen die Unterschiede bzw. Vor- und Nachteile der konservativen und der progressiven Approximation?\newline
\newline
Der Vorteil der progressiven Approximation ist meiner Meinung nach in der Verarbeitungsgeschwindigkeit zu sehen, jedoch hat diese auch zum Nachteil, dass einige möglicherweise richtige Treffer als Fehltreffer ermittelt werden.\newline
Die Vor- und Nachteile der konservativen Approximation jeweils bereits in der Beschreibung der progressiven Approximation zu sehen.

\subsubsection{Frage 3}
Formulieren Sie die Anforderungen an räumliche Indexstrukturen.\newline
\newline
Räumliche Indexstrukturen sollen die potenziellen Treffer vorfiltern, eine Methode nährungen an die Struktur zu erzeugen wurde bereits im Abschnitt ``Filterung über Approximationen'' beschrieben.\newline
Solche Approximation könnten wiederum als mehrelementige Approximationen genutzt werden ähnlich wie bei Baumstrukturen.

\subsubsection{Frage 4}
In diesem Abschnitt wurden mehrere Möglichkeiten für die Bildung eines Index durch Aufteilung der Ebene (des Raumes) vorgestellt (Regelmäßige Gitter, Baumstrukturen, Quadtrees, Adressfelder, Gridfile, z-Ordnung). Wählen Sie zwei dieser Möglichkeiten und vergleichen sie diese nach Vor- und Nachteilen miteinander.\newline
\newline
Der PR-Quadtree kann sehr schnell sehr viele kleine Quadrate beinhalten, auch ist die Frage wie es sich bei überlappenden Polygonen verhält nicht geklärt und kann unter umständen zu komplikationen führen.\newline
Vorteilhaft im vergleich zu Gridfiles ist die ``ansprache'' der einzelnen Zellen, diese ist beim Quadtree besser über einen Binärcode gelöst als bei den Gridfiles über die Matrix.

\subsubsection{Frage 5}
Welche praktischen Vor und Nachteile hat der R+-Baum gegenüber einem R-Baum?\newline
\newline
Durch die nicht vorhandene Überlappung sind die Suchgebiete kleiner und haben somit auch weniger Einträge welche es im zweiten Filterstufe zu betrachten gilt.\newline
Jedoch haben R+Bäume auch mehrere Knotenpunkte welche betrachtet werden müssen als R-Bäume, dies ist jedoch aufgrund der heutigen Hardware zu vernachlässigen.